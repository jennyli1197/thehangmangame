package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.Character.*;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private Path        workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        gamedata = new GameData(appTemplate);
        gameButton.setDisable(true);
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable, false);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);

        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable, false);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        if (success)
            dialog.show("Winning Message", "You win!");
        else
            dialog.show("Losing Message", "Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\".");
    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetWord = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetWord.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetWord[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
    }

    public void play() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    savable = true;
                    appTemplate.getGUI().updateWorkspaceToolbar(savable, true);
                    char guess = event.getCharacter().charAt(0);
                    if (isLetter(guess)) {
                        guess = toLowerCase(guess);
                        String c = new String(""+guess);
                        if (!alreadyGuessed(c)) {
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(c);
                                    goodguess = true;
                                    discovered++;
                                }
                            }
                            if (!goodguess)
                                gamedata.addBadGuess(c);

                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        }
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(String c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }
    
    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (savable)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable, false);
            enableGameButton();
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable, true);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable, false);
            enableGameButton();
        }

    }
    
    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            if (workFile != null)
                save(workFile);
            else {
                FileChooser fileChooser = new FileChooser();
                new File(APP_WORKDIR_PATH.getParameter()).mkdir();
                URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

                File initialDir = new File(workDirURL.getFile());
                initialDir.getParentFile().mkdirs();
                initialDir.mkdir();
                fileChooser.setInitialDirectory(initialDir);
                fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC), propertyManager.getPropertyValue(WORK_FILE_EXT)));
                File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    String name = selectedFile.getName();
                    String ext = name.substring(name.lastIndexOf(".") + 1);
                    if (ext.compareToIgnoreCase("json") != 0)
                        throw new IOException();
                    else
                        save(selectedFile.toPath());
                }
            }
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    @Override
    public void handleLoadRequest() {
        try {
            if (workFile != null) {
                appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), workFile);
                char[] targetWord = gamedata.getTargetWord().toCharArray();
                progress = new Text[targetWord.length];
                String data = new String(gamedata.getGoodGuesses().toString());
                int newDiscover = 0;

                for (int i = 0; i < progress.length; i++) {
                    progress[i] = new Text(Character.toString(targetWord[i]));
                    progress[i].setVisible(false);

                    for (int j = 0; j < data.length(); j++) {
                        if (isLetter(data.charAt(j)))
                            if (progress[i].getText().charAt(0) == data.charAt(j)) {
                                progress[i].setVisible(true);
                                newDiscover++;
                            }
                    }
                }
                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
                HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
                guessedLetters.getChildren().clear();
                guessedLetters.getChildren().addAll(progress);
                remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
                remainingGuessBox.getChildren().clear();
                remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
                discovered = newDiscover;

                savable = true;
                appTemplate.getGUI().updateWorkspaceToolbar(savable, false);

                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Load Message", "Game has been loaded.");
            }
            else {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                PropertyManager props  = PropertyManager.getManager();
                dialog.show(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
            }
        } catch (IOException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable)
                exit = promptToSaveExit();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    public void handleInfoRequest() {
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        dialog.show("Hangman", "The Hangman Game\nAuthor: Jenny Li");

    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            if (workFile != null)
                save(workFile);
            else {
                FileChooser fileChooser = new FileChooser();
                new File(APP_WORKDIR_PATH.getParameter()).mkdir();
                URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

                File initialDir = new File(workDirURL.getFile());
                initialDir.getParentFile().mkdirs();
                initialDir.mkdir();
                fileChooser.setInitialDirectory(initialDir);
                fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC), propertyManager.getPropertyValue(WORK_FILE_EXT)));
                File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    String name = selectedFile.getName();
                    String ext = name.substring(name.lastIndexOf(".") + 1);
                    if (ext.compareToIgnoreCase("json") != 0)
                        throw new IOException();
                    else
                        save(selectedFile.toPath());
                }
            }
        }

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    private boolean promptToSaveExit() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES)) {
            if (workFile != null)
                save(workFile);
            else {
                FileChooser fileChooser = new FileChooser();
                new File(APP_WORKDIR_PATH.getParameter()).mkdir();
                URL         workDirURL  = AppTemplate.class.getClassLoader().getResource(APP_WORKDIR_PATH.getParameter());
                if (workDirURL == null)
                    throw new FileNotFoundException("Work folder not found under resources.");

                File initialDir = new File(workDirURL.getFile());
                initialDir.getParentFile().mkdirs();
                initialDir.mkdir();
                fileChooser.setInitialDirectory(initialDir);
                fileChooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager.getPropertyValue(WORK_FILE_EXT_DESC), propertyManager.getPropertyValue(WORK_FILE_EXT)));
                File selectedFile = fileChooser.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    String name = selectedFile.getName();
                    String ext = name.substring(name.lastIndexOf(".") + 1);
                    if (ext.compareToIgnoreCase("json") != 0)
                        throw new IOException();
                    else
                        exitSave(selectedFile.toPath());
                }
            }
        }

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);

        workFile = target;
        savable = false;

        appTemplate.getGUI().updateWorkspaceToolbar(savable, true);

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    private void exitSave(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);

        workFile = target;
        savable = false;

        appTemplate.getGUI().updateWorkspaceToolbar(savable, true);
    }
}
