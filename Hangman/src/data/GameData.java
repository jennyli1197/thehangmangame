package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

import static settings.AppPropertyType.NEW_ERROR_TITLE;
import static settings.AppPropertyType.NEW_ERROR_MESSAGE;

/**
 * @author Ritwik Banerjee
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private Set<String> goodGuesses;
    private Set<String> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;

    public GameData(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.appTemplate.setDataComponent(this);
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
    }

    @Override
    public void reset() {
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            /*AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(NEW_ERROR_TITLE), props.getPropertyValue(NEW_ERROR_MESSAGE));
            */System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }

    public Set<String> getGoodGuesses() {
        return goodGuesses;
    }


    public GameData setGoodGuesses(Set<String> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<String> getBadGuesses() {
        return badGuesses;
    }


    public GameData setBadGuesses(Set<String> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public GameData setRemainingGuesses(int remainingGuesses) {
        this.remainingGuesses = remainingGuesses;
        return this;
    }

    public void addGoodGuess(String c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(String c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }


}
