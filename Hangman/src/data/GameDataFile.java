package data;

import components.AppDataComponent;
import components.AppFileComponent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static settings.AppPropertyType.*;

/**
 * @author Ritwik Banerjee
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) {
        JSONObject obj = new JSONObject();
        obj.put("Target Word", ((GameData) data).getTargetWord());

        JSONArray goodGuessesArr = new JSONArray();
        Iterator<String> iterateGood = ((GameData) data).getGoodGuesses().iterator();
        while (iterateGood.hasNext()) {
            goodGuessesArr.add(iterateGood.next());
        }
        JSONArray badGuessesArr = new JSONArray();
        Iterator<String> iterateBad = ((GameData) data).getBadGuesses().iterator();
        while (iterateBad.hasNext()) {
            badGuessesArr.add(iterateBad.next());
        }

        obj.put("Good Guesses", goodGuessesArr);
        obj.put("Bad Guesses", badGuessesArr);

        obj.put("Remaining Guesses", ((GameData) data).getRemainingGuesses());

        try {
            FileWriter file = new FileWriter(to.toFile());
            file.write(obj.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }

    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        JSONParser parser = new JSONParser();
        GameData gameData = ((GameData) data);
        try {
            Object obj = parser.parse(new FileReader(from.toFile()));
            JSONObject jsonObj = (JSONObject) obj;

            gameData.setTargetWord((String)jsonObj.get("Target Word"));
            JSONArray goodGuessArr = (JSONArray) jsonObj.get("Good Guesses");
            Iterator<String> iterateGood = goodGuessArr.iterator();
            Set<String> setGood = new HashSet<>();
            while (iterateGood.hasNext()) {
                setGood.add(iterateGood.next());
            }
            gameData.setGoodGuesses(setGood);
            JSONArray badGuessArr = (JSONArray) jsonObj.get("Bad Guesses");
            Iterator<String> iterateBad = badGuessArr.iterator();
            Set<String> setBad = new HashSet<>();
            while (iterateBad.hasNext()) {
                setBad.add(iterateBad.next());
            }
            gameData.setBadGuesses(setBad);
            long remainGuess = (long) jsonObj.get("Remaining Guesses");
            gameData.setRemainingGuesses((int)remainGuess);
        } catch (IOException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
        } catch (ParseException e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Load Error", "Oh no! Could not parse file.");
        }
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
